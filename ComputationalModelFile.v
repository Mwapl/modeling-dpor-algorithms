From Coq Require Import Arith.EqNat.
From Coq Require Import Init.Nat.
From Coq Require Import Arith.Arith.

Notation Aid := nat.

Create HintDb LTSGeneral.

Module Type ComputationModel.
  Parameter Action : Type.
  Parameter Proc : Action -> Aid.
  Parameter ModelState : Type.
  Parameter Enabled : Action -> ModelState -> Prop.
  Parameter Semantic : ModelState -> Action -> ModelState -> Prop.
  Parameter InitialState : ModelState.

  Inductive reachableState : ModelState -> Prop :=
  | Init :
    reachableState InitialState
  | Trans (s s' : ModelState) (a : Action) :
    Semantic s a s' ->
    reachableState s ->
    reachableState s'
  .

  Parameter nonBlockingSemantic :
    forall action : Action, forall s : ModelState,
      reachableState s ->
      ( Enabled action s <->
          exists s' : ModelState, Semantic s action s').

  Parameter ActorPersistence :
    forall s : ModelState,
      reachableState s ->
      forall a1 a2 : Action,
        Enabled a1 s ->
        Enabled a2 s ->
        Proc a1 <> Proc a2 ->
        forall s' : ModelState,
          Semantic s a1 s' ->
          Enabled a2 s'.
        
  Parameter Independent : Action -> Action -> Prop.
  Parameter indep_sym :
    forall a1 a2, Independent a1 a2 -> Independent a2 a1.
    
  Parameter indep_caracterization :
    forall a1 a2,
      Independent a1 a2 <->    
        (forall s: ModelState, (reachableState s) ->
                          (forall s' : ModelState,  Semantic s a1 s' -> (Enabled a2 s  <-> Enabled a2 s'))
                          /\ (forall s' : ModelState, Semantic s a2 s' -> (Enabled a1 s <-> Enabled a1 s' ))
                          /\ (forall s1 s2 : ModelState,
                                (Semantic s a1 s1 /\ Semantic s a2 s2) ->
                                exists s' : ModelState, (Semantic s2 a1 s' /\ Semantic s1 a2 s'))).
  
    
End ComputationModel.


Hint Extern 1 =>
       match goal with
         H : ?n1 <> ?n2 
         |- _ =>
           let new_name := fresh "falseBComp" in 
           apply Nat.eqb_neq in H as new_name; rewrite new_name; simpl (*; congruence*)
end
  : LTSGeneral.

Hint Extern 1 =>
       match goal with
         H : ?n1 <> ?n2 
         |- _ =>
           let new_name := fresh "falseBComp" in 
           apply not_eq_sym in H; apply Nat.eqb_neq in H as new_name; rewrite new_name; simpl(*; congruence*)
end
  : LTSGeneral.

Hint Extern 1 => 
       match goal with
       |         H : ?n1 = ?n2
                 |- _ =>
                   rewrite H; rewrite <- EqNat.beq_nat_refl_stt; simpl
|  |- context [?n1 =? ?n1] => rewrite <- EqNat.beq_nat_refl_stt; simpl
end
  : LTSGeneral.

Hint Extern 1 => congruence : LTSGeneral.

Hint Extern 5 =>
       match goal with
         |- context G [?n1 =? ?n2] =>
           let new_name := fresh "eq_dec" in 
           pose proof (Nat.eq_dec n1 n2) as new_name; destruct new_name; auto
end
  : LTSGeneral.


Hint Extern 2 =>
       match goal with
         diff_lemma : forall n, n <> ?n1 -> _,
  diff_hyp : ?n2 <> ?n1
  |- _ =>
  let new_name := fresh "diff" in
  apply diff_lemma in diff_hyp as new_name;
rewrite <- new_name in *
end
  : LTSGeneral.

Hint Extern 2 =>
       match goal with
         diff_lemma : forall n, n <> ?n1 -> _,
  diff_hyp : ?n1 <> ?n2
  |- _ =>
  let new_name := fresh "diff" in
  apply Nat.neq_sym in diff_hyp as new_name ; apply diff_lemma in new_name; rewrite <- new_name in * 
end
  : LTSGeneral.
