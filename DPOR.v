Require Import ComputationalModelFile.
Require Import LTSModel.

From Coq Require Import Lists.List.
From Coq Require Import Arith.PeanoNat.
Import ListNotations.

Require Import Coq.Sets.Ensembles.

Module DPORAlgorithm (Model : ComputationModel).

  Module LTSModel := LTS Model.
  Import LTSModel.
  
  Inductive happens_before : list Transition -> nat -> nat -> Prop :=
  | Dependence (S : list Transition) (i j : nat) (transition_i transition_j : Transition) :
    Execution S ->
    i <= j <= length S ->
    nth_error S i = Some transition_i ->
    nth_error S j = Some transition_j -> 
    Dependent (action_of_transition transition_i) (action_of_transition transition_j) -> 
    happens_before S i j
  | TransitivelyClosed (S : list Transition) (i j k : nat) :
    happens_before S i j ->
    happens_before S j k ->
    happens_before S i k.

  Notation " i '-->[' S ']' j " := (happens_before S i j) (at level 30, S at next level).

  Inductive happens_before_process : list Transition -> nat -> Aid -> Prop :=
  | Proc_i_is_p (S : list Transition) (i : nat) (p : Aid) (transition_i : Transition) :
    Execution S ->
    i <= length S ->
    nth_error S i = Some transition_i ->
    Model.Proc (action_of_transition transition_i) = p ->
    happens_before_process S i p
  | Exists_Intermediate (S : list Transition) (p : Aid) (i k : nat) (transition_k : Transition) :
    i < k <= length S ->
    i -->[S] k ->
    nth_error S k = Some transition_k ->
    Model.Proc (action_of_transition transition_k) = p ->
    happens_before_process S i p
  .

  (* Definition DPOR (C : ProgramCode) : Ensemble (list Transition) := *)
    
  (*   Fixpoint Explore (S : list Transition) : Ensemble (list Model.Action) := *)
  (*     let last_transition := last S in *)
  (*     let (_, s) := last_transition in *)
  (*     if (exists i, i <= length S *)
  (* . *)
                          
  Parameter Algorithm : ProgramCode -> Ensemble (list Transition).

  Parameter ExploreExecution :
    forall code, forall l,
      In (list Transition) (Algorithm code) l -> Execution l. 

  Parameter Soundness :
    forall code, forall u,
      MaximalExecution u ->
      exists v, In (list Transition) (Algorithm code) v /\
             u simeq v.

  Parameter Optimality :
    forall code, forall u v,
      u simeq v ->
      In (list Transition) (Algorithm code) u ->
      ~ In (list Transition) (Algorithm code) v.

End DPORAlgorithm.
