From Modeling Require Import ComputationalModelFile.
Require Import List.
Import ListNotations.
From Coq Require Import Arith.PeanoNat.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Classes.RelationClasses.

Create HintDb Mutex.

Module MutexModel.
  Inductive MutexAction : Type :=
  | MutexLock (actor : Aid) (mutex : nat)
  | MutexUnlock (actor : Aid) (mutex : nat)
  | MutexWait (actor : Aid) (mutex : nat)
  .
  Definition Action := MutexAction.
  
  Definition Proc (a : Action) : Aid :=
    match a with
    | MutexLock a m => a
    | MutexUnlock a m => a
    | MutexWait a m => a
    end
  .
  
  Notation MutexId := nat.
  
  Record MutexState :=  {
    Ownership : MutexId -> option Aid;
    Waiting : MutexId -> (list Aid);
  }
  .
  
  Definition ModelState := MutexState.
  Inductive MutexEnabled : Action -> ModelState -> Prop :=
  | EnabledLock a m s :
    (Ownership s) m <> Some a ->
    ~ (List.In a (Waiting s m))  ->
    MutexEnabled (MutexLock a m) s  
  | EnabledUnlock a m s :
    (Ownership s) m = Some a ->
    MutexEnabled (MutexUnlock a m) s
  | EnabledWait a m s :
    (Ownership s) m = Some a ->
    MutexEnabled (MutexWait a m) s              
  .

  Definition Enabled := MutexEnabled.

  Global Hint Unfold Enabled : Mutex.
  
  Definition identicalApartFromIdOwnership (f1 f2 : MutexId -> option Aid) (m : MutexId) : Prop :=
    forall n : nat, n <> m -> f1(n) = f2(n).
  
  Definition identicalApartFromIdWaiting (f1 f2 : MutexId -> list Aid) (m : MutexId) : Prop :=
    forall n : nat, n <> m -> f1(n) = f2(n).

  Hint Unfold identicalApartFromIdOwnership : Mutex.
  Hint Unfold identicalApartFromIdWaiting : Mutex.
  
  Inductive MutexSemantic : ModelState -> Action -> ModelState -> Prop :=    

  | SemanticLockNotOwned (actor : Aid) (mutex : MutexId) (s s' : ModelState) :
    (Ownership s) mutex = None ->
    (Waiting s) mutex = nil ->
    (Ownership s') mutex = Some actor ->
    (Waiting s') = (Waiting s) ->
    identicalApartFromIdOwnership (Ownership s) (Ownership s') mutex ->
    MutexSemantic s (MutexLock actor mutex) s'

  | SemanticLockOwned (actor actor' : Aid) (mutex : MutexId) (s s' : ModelState) (waiters : list Aid) :
    actor <> actor' ->
    ~ (List.In actor (Waiting s mutex)) ->
    (Ownership s) mutex = Some actor' ->
    (Waiting s) mutex = waiters ->
    (Ownership s') = (Ownership s)  ->
    (Waiting s') mutex = (actor::waiters) ->
    identicalApartFromIdWaiting (Waiting s) (Waiting s') mutex ->
    MutexSemantic s (MutexLock actor mutex) s'
  
  | SemanticWait (actor : Aid) (mutex : MutexId) (s : ModelState):
    (Ownership s) mutex = Some actor ->
    MutexSemantic s (MutexWait actor mutex) s

  | SemanticUnlockNoWaiter (actor : Aid) (mutex : MutexId) (s s' : ModelState) :
    (Ownership s) mutex = Some actor ->
    (Waiting s) mutex = nil ->
    (Ownership s') mutex = None ->
    (Waiting s') = (Waiting s) ->
    identicalApartFromIdOwnership (Ownership s) (Ownership s') mutex ->
    MutexSemantic s (MutexUnlock actor mutex) s'

                  
  | SemanticUnlockWaiter (actor : Aid) (mutex : MutexId) (s s' : ModelState) (fwait : Aid) (waiters : list Aid) :
    (Ownership s) mutex = Some actor ->
    (Waiting s) mutex = (fwait::waiters) ->
    (Ownership s') mutex = Some (last waiters fwait) ->
    (Waiting s') mutex = (removelast (fwait::waiters)) ->
    identicalApartFromIdOwnership (Ownership s) (Ownership s') mutex ->
    identicalApartFromIdWaiting (Waiting s) (Waiting s') mutex ->
    MutexSemantic s (MutexUnlock actor mutex) s'

  .
  Definition Semantic := MutexSemantic.

  Definition mutex_of_action (action : Action) : nat :=
    match action with
    | MutexLock _ m => m
    | MutexWait _ m => m
    | MutexUnlock _ m => m
    end
  .

  Definition emptyOwnership (m : MutexId) : option Aid := None.
  Definition emptyWaiting (m : MutexId) : list Aid  :=  nil.

  Definition InitialState : MutexModel.ModelState :=
    {|
      Ownership := emptyOwnership; 
      Waiting := emptyWaiting;
    |}
  .

  Inductive reachableState : ModelState -> Prop :=
  | Init :
    reachableState InitialState
  | Trans (s s' : ModelState) (a : Action) :
    Semantic s a s' ->
    reachableState s ->
    reachableState s'
  .

  Lemma last_remove_first :
    forall (e1 e2 : nat) l default,
      last (e1::e2::l) default = last (e2::l) default.
  Proof.
    intros. unfold last. reflexivity.
  Qed. 
  
  Lemma not_in_list_imply_not_last :
    forall (h e : nat) (q : list nat),
      (~ List.In e (h::q))
      -> e <> last q h.
  Proof.
    intros h e q.
    generalize dependent e. generalize dependent h.
    induction q; intros.
    - unfold not in H.
      simpl in H. simpl. assert (e <> h); auto.
    - apply not_in_cons in H; destruct H.
      apply not_in_cons in H0; destruct H0.
      destruct q.
      -- simpl. simpl in H. pose proof (Nat.eq_dec e a); auto.
      -- rewrite last_remove_first. apply IHq.
         simpl. unfold not. intros. destruct H2; auto.
  Qed.

  Lemma not_in_list_not_in_sublist :
    forall (h e : nat) (q : list nat),
      (~ List.In e q)
      -> ~ List.In e (removelast q).
  Proof.
    intros. generalize dependent h; generalize dependent e.
    induction q; intros.
    - simpl. auto.
    - destruct q; auto.
      pose proof (nil_cons). specialize (H0 nat n q). apply neq_Symmetric in H0.
      apply removelast_app with (l:=[a]) in H0. assert ([a]++n::q = a::n::q). { simpl. auto. }
      rewrite H1 in H0. rewrite H0. apply not_in_cons. split.
      -- simpl in H. unfold not in *. intro. apply H. left. auto.
      -- apply IHq. unfold not in *. intro. apply H. simpl. right. simpl in H2. auto. auto.
  Qed.
  
  Theorem ActorPersistence :
    forall s : ModelState,
      reachableState s ->
      forall a1 a2 : Action,
        Enabled a1 s ->
        Enabled a2 s ->
        Proc a1 <> Proc a2 ->
        forall s' : ModelState,
          Semantic s a1 s' ->
          Enabled a2 s'.
  Proof.
    intros.
    destruct a1; destruct a2; inversion H0; inversion H1;
      inversion H3; constructor; autounfold with Mutex in *; simpl in *; auto with Mutex LTSGeneral;
      pose proof (Nat.eq_dec mutex mutex0) as same_mutex; destruct same_mutex; auto with Mutex LTSGeneral.
    - rewrite e in *. rewrite H22. simpl. unfold not. intro. destruct H25; auto. congruence.
    - rewrite e in *. rewrite H17. rewrite H16 in H12. apply not_in_list_imply_not_last in H12. congruence.
    - rewrite e in *. rewrite H16 in H12. apply not_in_list_not_in_sublist in H12; auto. simpl in H12. congruence.
  Qed.
 
  Lemma notLockedImpliesNoWaiting :
    forall s : ModelState, forall mutex : MutexId,
      reachableState s ->
      (Ownership s) mutex = None ->
      (Waiting s) mutex = nil.
  Proof.
    intros. generalize dependent mutex. induction H; intros.
    + unfold InitialState. simpl. unfold emptyWaiting. auto.
    + inversion  H.
      ++ (* AsyncLockNotOwned *)
        rewrite H5.
         pose proof (Nat.eq_dec mutex mutex0) as same_or_diff_mutexes.
         destruct same_or_diff_mutexes. 

         apply IHreachableState in H2. rewrite e in *. rewrite H2 in *. assumption.
         unfold identicalApartFromIdOwnership in *.
         apply H6 in n as same_ownership. 
         rewrite <- same_ownership in H1. apply IHreachableState in H1.
         assumption.
      ++ (* AsyncLockOwned *)
        pose proof (Nat.eq_dec mutex mutex0) as same_or_diff_mutexes.
         destruct same_or_diff_mutexes. rewrite e in *. rewrite H5 in *. congruence.
         unfold identicalApartFromIdWaiting in *.  
         apply H8 in n as same_waiting.
         rewrite <- same_waiting in *. apply IHreachableState. congruence. 
      ++ (* Wait *)
        rewrite H5 in *. apply IHreachableState. assumption.
      ++ (* UnlockNoWaiters *)
        pose proof (Nat.eq_dec mutex mutex0) as same_or_diff_mutexes.
        destruct same_or_diff_mutexes. rewrite e in *. rewrite H5. assumption.
         unfold identicalApartFromIdOwnership in *.
         apply H6 in n as same_ownership. 
         rewrite <- same_ownership in H1. apply IHreachableState in H1. rewrite H5 in *.
         assumption.
      ++ (* UnlockWaiters *)
        pose proof (Nat.eq_dec mutex mutex0) as same_or_diff_mutexes.
        destruct same_or_diff_mutexes. rewrite e in *. rewrite H4 in *. inversion H1.
        unfold identicalApartFromIdOwnership in *. unfold identicalApartFromIdWaiting in *.
        apply H6 in n as same_ownership. apply H7 in n as same_waiting.
        rewrite <- same_waiting. apply IHreachableState. rewrite same_ownership. assumption.
  Qed.

  Global Hint Resolve notLockedImpliesNoWaiting : Mutex.
  
  Theorem nonBlockingSemantic :
    forall action : Action, forall s : ModelState,
      reachableState s ->
      ( Enabled action s <->
          exists s' : ModelState, Semantic s action s').
  Proof.
    intros. split. destruct action; intros; inversion H0.
    + case_eq (Ownership s mutex); intros.
      ++ (* AsyncLock when mutex is already locked *)
        exists {| Ownership := Ownership s;
            Waiting := fun mutex => if mutex =? m then actor::((Waiting s) mutex) else (Waiting s) mutex |}.
        rewrite H2 in *.
        apply SemanticLockOwned with (actor' := n) (waiters := (Waiting s) mutex); autounfold with Mutex in *;
          intros; simpl in *;  auto with Mutex LTSGeneral.      
      ++ (* Asynclock when mutex is free *)
        exists {| Ownership := fun mutex => if mutex=? m then Some actor else (Ownership s) mutex;
            Waiting :=  (Waiting s) |}. rewrite H2 in *.
          apply SemanticLockNotOwned; autounfold with Mutex in * ; simpl in *; intros; auto with Mutex LTSGeneral.         
    + (* Unlock *)
      case_eq ((Waiting s) mutex) ; intros.
      ++ (* if no one is waiting after *)
        exists {| Ownership := fun mutex => if mutex =? m then None else (Ownership s) mutex;
            Waiting := Waiting s |}. rewrite H3 in *.
        apply SemanticUnlockNoWaiter; autounfold with Mutex in *; simpl in *;
          intros; auto with Mutex LTSGeneral.
      ++ (* if there are waiters *)
          exists {| Ownership := fun mutex => if mutex =? m then Some (last l n) else (Ownership s) mutex;
            Waiting := fun mutex => if mutex =? m then removelast (n::l) else (Waiting s) mutex |}.
          rewrite H3 in *. apply SemanticUnlockWaiter with (fwait:=n)(waiters:=l); 
          autounfold with Mutex in *; simpl in *;
            intros; auto with Mutex LTSGeneral.
    + (* Wait *)
      exists s. apply (SemanticWait actor mutex s). assumption.
    + (* reciprocal *)
      intros. destruct H0. inversion H0 ; unfold Enabled; constructor; auto with Mutex LTSGeneral.
      rewrite H2. auto.
  Qed.

  Definition Commute (a1 a2 : Action) :=
    forall s s1 s2,
      Semantic s a1 s1 /\ Semantic s a2 s2 ->
      exists s', Semantic s1 a2 s' /\ Semantic s2 a1 s'. 

  Local Hint Unfold Commute : Mutex.

  Lemma commute_sym :
    forall a1 a2,
      Commute a1 a2 -> Commute a2 a1.
  Proof.
    intros. unfold Commute in *. intros.
    specialize (H s s2 s1). apply and_comm in H0. apply H in H0.
    destruct H0. exists x. destruct H0. auto.
  Qed.

  Theorem commute_wait_wait :
    forall m1 p1 m2 p2,
      Commute (MutexWait p1 m1) (MutexWait p2 m2).
  Proof.
    intros. unfold Commute. intros. destruct H. inversion H. inversion H0.
    exists s. split; rewrite <- H3 in *; rewrite <- H8 in *; constructor; auto.
  Qed.

  Local Hint Extern 4 =>
          match goal with
            |- context G [(fun n => _) = _] =>
              apply functional_extensionality; intros
  end
: Mutex.
  
  Lemma commute_diff_mutex_lock_lock : 
    forall m1 p1 m2 p2,
      m1 <> m2 ->
      Commute (MutexLock p1 m1) (MutexLock p2 m2).
  Proof.
    intros. unfold Commute. intros.
    destruct H0 as [transition1 transition2].
    (* AsyncLcok m1, Asynclock m2 *)
    inversion transition1; inversion transition2.
    - (* no mutexes were locked *)
      exists {| Ownership := fun m => if m =? m1 then Some p1
                              else if m =? m2 then Some p2
                                   else Ownership s m
        ; Waiting := Waiting s |}. 
      split; constructor; try rewrite H6 in *; try rewrite H15 in *;
        autounfold with Mutex in *; simpl in *; intros; auto with Mutex LTSGeneral.
    - (* m1 free, m2 locked *) 
      exists {| Ownership := fun m => if m =? m1 then Some p1
                                                   
                              else Ownership s m
        ; Waiting := fun m => if m=? m2 then p2::waiters
                           else Waiting s m|}. split.
      -- apply SemanticLockOwned with (actor':=actor')(waiters:=waiters); simpl;
           autounfold with Mutex in *;
           try rewrite H6 in *;
           try rewrite H14 in *;
           auto with Mutex LTSGeneral. 
      -- constructor; simpl; autounfold with Mutex in *;
           try rewrite H6 in *; try rewrite H15 in *; auto with Mutex LTSGeneral.
    - (* m1 locked, m2 free *)
      exists {| Ownership := fun m => if m =? m2 then Some p2
                                                   
                              else Ownership s m
        ; Waiting := fun m => if m=? m1 then p1::waiters
                           else Waiting s m|}. split.
      -- constructor; simpl; autounfold with Mutex in *;
         try rewrite H6 in *; try rewrite H17 in *;
           auto with Mutex LTSGeneral.
        
      -- apply SemanticLockOwned with (actor':=actor')(waiters:=waiters); simpl;
           autounfold with Mutex in *;
           try rewrite H6 in *; try rewrite H17 in *;
           auto with Mutex LTSGeneral.
    - (* m1 locked, m2 locked *)
      exists {| Ownership := Ownership s
        ; Waiting := fun m => if m=? m1 then p1::waiters
                           else if m=?m2 then p2::waiters0  
                                else Waiting s m|}. split; auto.
      -- apply SemanticLockOwned with (actor':=actor'0)(waiters:=waiters0); simpl;
           autounfold with Mutex in *;
           try rewrite H6 in *; try rewrite H17 in *;
           auto with Mutex LTSGeneral.
      -- apply SemanticLockOwned with (actor':=actor')(waiters:=waiters); simpl;
           autounfold with Mutex in *;
           try rewrite H6 in *; try rewrite H17 in *;
           auto with Mutex LTSGeneral.
  Qed.
  
  Lemma commute_diff_mutex_lock_unlock :
    forall m1 p1 m2 p2,
      m1 <> m2 ->
      Commute (MutexLock p1 m1) (MutexUnlock p2 m2).
  Proof.
    unfold Commute. intros.
    destruct H0 as [transition1 transition2].
    inversion transition1; inversion transition2; autounfold with Mutex in *.
    - (* m1 free, no waiters on m2 *)
      exists {| Ownership := fun m => if m =? m1 then Some p1              
                                else if m=? m2 then None 
                                     else  Ownership s m
          ; Waiting := Waiting s |}. split.
      -- apply SemanticUnlockNoWaiter; intros; simpl; autounfold with Mutex in *;
           try rewrite H6 in *;
           auto with Mutex LTSGeneral.
      -- constructor; autounfold with Mutex in *; intros; simpl; try rewrite H15 in *; auto with Mutex LTSGeneral.
    - (* m1 free, waiters on m2 *)
      exists {| Ownership := fun m => if m =? m1 then Some p1              
                              else if m=? m2 then Some (last waiters fwait) 
                                   else  Ownership s m
        ; Waiting := fun m => if m =? m2 then removelast (fwait::waiters)
                           else Waiting s m|}. split.
      -- apply SemanticUnlockWaiter with (fwait:=fwait)(waiters:=waiters);
           autounfold with Mutex in *; intros; simpl; try rewrite H6 in *; auto with Mutex LTSGeneral.
      -- constructor;
           autounfold with Mutex in *; intros; simpl; try rewrite H6 in *; auto with Mutex LTSGeneral.
    - (* m1 locked, no waiter on m2 *)
      exists {| Ownership := fun m => if m =? m2 then None 
                                   else  Ownership s m
        ; Waiting := fun m => if m =? m1 then p1::waiters
                           else Waiting s m|}. split.
      -- apply SemanticUnlockNoWaiter;
           autounfold with Mutex in *; intros; simpl; try rewrite H6 in *; auto with Mutex LTSGeneral.
      -- apply SemanticLockOwned with (actor':=actor')(waiters:=waiters);
           autounfold with Mutex in *; intros; simpl; try rewrite H6 in *;
           try rewrite H17 in *; auto with Mutex LTSGeneral.
    - (* m1 locked, waiters on m2 *)
      exists {| Ownership := fun m => if m=? m2 then Some (last waiters0 fwait) 
                              else  Ownership s m
        ; Waiting := fun m => if m =? m1 then p1::waiters
                           else if m =? m2 then removelast (fwait::waiters0)
                           else Waiting s m|}. split.
      -- apply SemanticUnlockWaiter with (fwait:=fwait)(waiters:=waiters0);
           autounfold with Mutex in *; intros; simpl; try rewrite H6 in *; auto with Mutex LTSGeneral.
      -- apply SemanticLockOwned with (actor':=actor')(waiters:=waiters);
           autounfold with Mutex in *; intros; simpl; try rewrite H6 in *; auto with Mutex LTSGeneral.
  Qed.

  Lemma commute_diff_mutex_lock_wait :
    forall m1 p1 m2 p2,
      m1 <> m2 ->
      Commute (MutexLock p1 m1) (MutexWait p2 m2).
  Proof.
    unfold Commute. intros.
    destruct H0 as [transition1 transition2].
    inversion transition1; inversion transition2; autounfold with Mutex in *.
    - (* m1 free *)
      exists s1. split.
      -- constructor. apply Nat.neq_sym in H. apply H8 in H. congruence.
      -- congruence.
    - (* m1 locked *)
      exists s1. split.
      -- constructor. apply Nat.neq_sym in H. apply H10 in H. congruence.
      -- congruence.
  Qed.

  Lemma commute_diff_mutex_unlock_unlock :
    forall m1 p1 m2 p2,
      m1 <> m2 ->
      Commute (MutexUnlock p1 m1) (MutexUnlock p2 m2).
  Proof.
    unfold Commute. intros.
    destruct H0 as [transition1 transition2].
    inversion transition1; inversion transition2; autounfold with Mutex in *.
    - (* no waiters ; no waiters *)
      exists {| Ownership := fun m => if m =? m1 then None
                              else if m =? m2 then None
                                   else Ownership s m
        ; Waiting := Waiting s |}. split;
        constructor; autounfold with Mutex in *; intros; simpl; try rewrite H6 in *;
        try rewrite H15 in* ; auto with Mutex LTSGeneral.
    - (* no waiters; waiters *)
       exists {| Ownership := fun m => if m =? m1 then None
                              else if m =? m2 then Some (last waiters fwait)
                                   else Ownership s m
         ; Waiting := fun m => if m =?m2 then removelast (fwait::waiters)
                            else Waiting s m |}. split.
       -- apply SemanticUnlockWaiter with (fwait:=fwait)(waiters:=waiters);
          autounfold with Mutex in *; intros; simpl; try rewrite H6 in *; auto with Mutex LTSGeneral.
       -- apply SemanticUnlockNoWaiter;
            autounfold with Mutex in *; intros; simpl; try rewrite H6 in *; auto with Mutex LTSGeneral.
    - (* waiters ; no waiters *)
      exists {| Ownership := fun m => if m =? m2 then None
                              else if m =? m1 then Some (last waiters fwait)
                                   else Ownership s m
         ; Waiting := fun m => if m =?m1 then removelast (fwait::waiters)
                            else Waiting s m |}. split.
      -- apply SemanticUnlockNoWaiter;
           autounfold with Mutex in *; intros; simpl; try rewrite H6 in *; auto with Mutex LTSGeneral.
      -- apply SemanticUnlockWaiter with (fwait:=fwait)(waiters:=waiters);
           autounfold with Mutex in *; intros; simpl; try rewrite H6 in *;
           try rewrite H16 in *; auto with Mutex LTSGeneral.
    - (* waiters ; waiters *)
      exists {| Ownership := fun m => if m =? m2 then Some (last waiters0 fwait0)
                              else if m =? m1 then Some (last waiters fwait)
                                   else Ownership s m
        ; Waiting := fun m => if m =?m1 then removelast (fwait::waiters)
                           else if m=? m2 then removelast (fwait0::waiters0)
                                else Waiting s m |}. split.
      --  apply SemanticUnlockWaiter with (fwait:=fwait0)(waiters:=waiters0);
            autounfold with Mutex in *; intros; simpl; try rewrite H6 in *; auto with Mutex LTSGeneral.
      -- apply SemanticUnlockWaiter with (fwait:=fwait)(waiters:=waiters);
           autounfold with Mutex in *; intros; simpl; try rewrite H6 in *; auto with Mutex LTSGeneral.
  Qed.

  Lemma commute_diff_mutex_unlock_wait :
    forall m1 p1 m2 p2,
      m1 <> m2 ->
      Commute (MutexUnlock p1 m1) (MutexWait p2 m2).
  Proof.
    unfold Commute. intros.
    destruct H0 as [transition1 transition2].
    inversion transition1; inversion transition2; autounfold with Mutex in *.
    - (* no waiters *)
      exists s1. rewrite <- H11. split; auto.
      -- constructor; autounfold with Mutex; intros; simpl; try rewrite H11 in *; auto with Mutex LTSGeneral.
    - (* waiters *)
      exists s1. rewrite <- H12. split; auto.
      -- constructor; rewrite H12 in *; auto with Mutex LTSGeneral.
  Qed.

  Global Hint Resolve commute_sym : Mutex.
  Global Hint Resolve commute_diff_mutex_lock_lock: Mutex.
  Global Hint Resolve commute_diff_mutex_lock_unlock: Mutex.
  Global Hint Resolve commute_diff_mutex_lock_wait: Mutex.
  Global Hint Resolve commute_diff_mutex_unlock_wait: Mutex.
  Global Hint Resolve commute_diff_mutex_unlock_unlock: Mutex.
  Global Hint Resolve commute_wait_wait: Mutex.
  
  Theorem commute_different_mutexes :
    forall action1 action2,
      mutex_of_action action1 <> mutex_of_action action2 ->
      Commute action1 action2. 
  Proof.
    intros. 
    destruct action1, action2; unfold mutex_of_action in *; auto with Mutex LTSGeneral.
  Qed.

  Local Hint Resolve commute_different_mutexes : Mutex.
  
  Lemma same_last :
    forall (n : nat) l default1 default2,
      last (n::l) default1 = last (n::l) default2.
  Proof.
    intros. generalize dependent n.
    induction l.
    - auto.
    - intros. simpl. unfold last in IHl.
      apply IHl.
  Qed.

  Lemma commute_lock_unlock :
    forall m1 p1 m2 p2,
      Commute (MutexLock p1 m1) (MutexUnlock p2 m2).
  Proof.
    intros. pose proof (Nat.eq_dec m1 m2).
    destruct H as [same_mutex | diff_mutex]; auto with Mutex LTSGeneral.
    unfold Commute. intros.
    destruct H as [t1 t2].
    inversion t1; inversion t2; autounfold with Mutex in *; rewrite same_mutex in *; auto with Mutex LTSGeneral.
    - (* no other waiters *)
      exists {| Ownership := fun m => if m =? m2 then Some p1
                              else Ownership s m;
          Waiting := fun m => if m =? m2 then nil
                           else Waiting s m |}. split.
      -- apply SemanticUnlockWaiter with (fwait:=p1)(waiters:=nil);
           autounfold with Mutex; intros; simpl; auto with Mutex LTSGeneral. 
      -- apply SemanticLockNotOwned;
           autounfold with Mutex; intros; simpl; auto with Mutex LTSGeneral.
    - (* other waiters *)
      exists {| Ownership := fun m => if m =? m2 then Some (last (fwait::waiters0) p1)
                              else Ownership s m;
          Waiting := fun m => if m =? m2 then p1 :: (removelast (fwait::waiters0))
                           else Waiting s m |}. split.
      -- apply SemanticUnlockWaiter with (fwait:= p1)(waiters:= fwait::waiters0);
           autounfold with Mutex; intros; simpl; auto with Mutex LTSGeneral.
      -- apply SemanticLockOwned with (actor':= (last (waiters0) fwait))
                                      (waiters:=removelast (fwait :: waiters0))
         ; autounfold with Mutex; intros; simpl; auto with Mutex LTSGeneral.
         --- destruct (waiters0). autounfold in H2. rewrite H13 in H2. simpl in H2.
             pose proof (Nat.eq_dec p1 fwait) as decidable; destruct decidable; auto.
             rewrite H13 in H2. apply not_in_list_imply_not_last in H2.
             unfold not. intro. rewrite H20 in *.
             pose proof (same_last n l p1 fwait). congruence.
         --- destruct (waiters0). rewrite H15. auto.
             rewrite H13 in H2. apply not_in_list_not_in_sublist in H2; congruence.
         --- apply functional_extensionality. intro. pose proof (Nat.eq_dec x m2).
             destruct H20; auto with Mutex LTSGeneral. destruct waiters0; auto with Mutex LTSGeneral.
             pose proof (same_last n waiters0 p1 fwait). 
             rewrite e; rewrite <- EqNat.beq_nat_refl_stt. congruence.
  Qed.

  Global Hint Resolve commute_lock_unlock : Mutex.
  
  Lemma commute_lock_wait :
    forall m1 p1 m2 p2,
      Commute (MutexLock p1 m1) (MutexWait p2 m2).
  Proof.
    intros. intros. pose proof (Nat.eq_dec m1 m2).
    destruct H as [same_mutex | diff_mutex]; auto with Mutex LTSGeneral.
    unfold Commute. intros.
    destruct H as [t1 t2].
    inversion t1; inversion t2; autounfold with Mutex in *; rewrite same_mutex in *; try congruence.
    rewrite H12 in *.
    exists s1. split.
    - constructor. rewrite H5. assumption.
    - apply SemanticLockOwned with (actor':=actor')(waiters:=waiters);
        autounfold with Mutex; simpl; intros; auto with Mutex LTSGeneral.
  Qed.

  Global Hint Resolve commute_lock_wait : Mutex.
  
  Lemma independent_different_mutexes :
    forall s s1 a1 a2,
      mutex_of_action a1 <> mutex_of_action a2 ->
      Semantic s a1 s1 -> (
      Enabled a2 s <->
      Enabled a2 s1). 
  Proof.
    intros. split; intros;
    destruct a1; destruct a2; constructor; autounfold with Mutex in *; simpl in *;
      inversion H0; inversion H1; autounfold with Mutex in *; auto with Mutex LTSGeneral.
    apply Nat.neq_sym in H; try apply H9 in H; rewrite H in *; congruence.
  Qed.

  Inductive mutexIndependent : Action -> Action -> Prop :=
  | DifferentMutexes (a1 a2 : Action) :
    mutex_of_action a1 <> mutex_of_action a2 ->
    mutexIndependent a1 a2
  | LockUnlock (m1 m2 : MutexId) (p1 p2 : Aid) :
    p1 <> p2 ->
    mutexIndependent (MutexLock p1 m1) (MutexUnlock p2 m2)
  | LockWait (m1 m2 : MutexId) (p1 p2 : Aid) :
    p1 <> p2 ->
    mutexIndependent (MutexLock p1 m1) (MutexWait p2 m2)
  | WaitWait (m1 m2 : MutexId) (p1 p2 : Aid) :
    mutexIndependent (MutexWait p1 m1) (MutexWait p2 m2)
  | Symmetrical (a1 a2 : Action) :
    mutexIndependent a1 a2 ->
    mutexIndependent a2 a1
  .
  
  Definition Independent := mutexIndependent.

  Lemma indep_sym :
    forall a1 a2, Independent a1 a2 -> Independent a2 a1.
  Proof.
    intros. apply Symmetrical; auto.
  Qed.

  Lemma indep_imply_comm :
    forall a1 a2, Independent a1 a2 -> Commute a1 a2.
  Proof.
    intros. induction H; auto with Mutex LTSGeneral.
  Qed. 

  Local Hint Extern 2 =>
    match goal with
      And : ?H1 /\ ?H2
    |- _ =>
        let new_name1 := fresh "H" in
        let new_name2 := fresh "H" in 
  destruct And as [new_name1 new_name2] 
  end
          : Mutex.

                                               
  Local Hint Extern 2 =>
          match goal with 
            AndLemma:?H1 /\ ?H2 -> exists x, _,
    Hyp1:?H1,
    Hyp2:?H2
    |- exists x, _ =>
    let new_H := fresh "H" in
    let new_variable := fresh "x" in
    assert (new_H : H1 /\ H2); auto; apply AndLemma in new_H;
  destruct new_H as [new_variable new_H]; exists new_variable 
                                            end
    : Mutex.

   Lemma in_list_in_sublits :
     forall (A: Type) (a : A)  (l1 l2 : list A),
       List.In a (l1++l2) <->
       (List.In a l1) \/ (List.In a l2).
   Proof.
     intros; split.
     - generalize dependent l2. 
       induction l1; intros; simpl in *; auto.
       destruct H; auto.
       apply IHl1 in H.
       destruct H; auto.
     - generalize dependent l2.
       induction l1; intros; simpl in *. destruct H; auto; contradiction.
       destruct H; auto.
       destruct H; auto.
   Qed.
                                            
   Theorem indep_caracterization :
     forall a1 a2,
       Independent a1 a2 <->    
         (forall s: ModelState, (reachableState s) ->
                           (forall s' : ModelState,  Semantic s a1 s' -> (Enabled a2 s  <-> Enabled a2 s'))
                           /\ (forall s' : ModelState, Semantic s a2 s' -> (Enabled a1 s <-> Enabled a1 s' ))
                           /\ (forall s1 s2 : ModelState,
                                 (Semantic s a1 s1 /\ Semantic s a2 s2) ->
                                 exists s' : ModelState, (Semantic s2 a1 s' /\ Semantic s1 a2 s'))).
   Proof.
     intros action1 action2. split. 
     - (* --> *)
      intro. apply indep_sym in H as commute_actions.
      apply indep_imply_comm in commute_actions.
      induction H; intros; split; split; intros; try (split; intros);
        autounfold with Mutex in *;
        try (rewrite <- independent_different_mutexes; eauto; fail);
        try (rewrite independent_different_mutexes; eauto; fail);
        try (eapply commute_actions; eauto with Mutex LTSGeneral; fail);
        try (inversion H2; constructor; inversion H1;
             autounfold with Mutex in *; pose proof (Nat.eq_dec m2 m1) as dec;
             destruct dec as [dec | dec];
             try (rewrite dec in *); auto with Mutex LTSGeneral; fail).

             -- 
               inversion H2; constructor; inversion H1;
                 autounfold with Mutex in *; pose proof (Nat.eq_dec m2 m1) as dec;
                 destruct dec as [dec | dec]; try (rewrite dec in *); auto with Mutex LTSGeneral.
               unfold not; intros.
               rewrite H18 in *.
               rewrite H11 in H7.
               apply not_in_list_imply_not_last in H7. 
               congruence.
               apply not_in_list_not_in_sublist in H7; auto.
               congruence.
             -- 
               inversion H2; constructor; inversion H1;
                 autounfold with Mutex in *; pose proof (Nat.eq_dec m2 m1) as dec;
                 destruct dec as [dec | dec]; try (rewrite dec in *); auto with Mutex LTSGeneral.
               apply Nat.neq_sym in dec. apply H15 in dec. congruence.
               rewrite H11. simpl. unfold not; intros.
               rewrite H13 in H7. destruct H18. rewrite H18 in *.
               destruct waiters. simpl in *. congruence.
               assert (p1 :: n :: waiters = [p1] ++ (n::waiters)). { auto. }
               rewrite H19 in H7. rewrite removelast_app in H7.
               simpl in H7. destruct H7. left. reflexivity.
               pose proof (nil_cons). specialize (H20 nat n waiters). congruence.
               destruct H7. pose proof (Nat.eq_dec (last waiters fwait) p1).
               destruct H7; try congruence. apply In_split in H18.
               destruct H18 as [l1 [l2 waiters_decomposition]].
               rewrite waiters_decomposition in *.
               destruct l2. rewrite last_last in n. contradiction.
               assert (fwait :: l1 ++ p1 :: n0 :: l2 = (fwait :: l1 ++ [p1]) ++ (n0 :: l2)).
               { simpl. rewrite <- app_assoc. simpl. reflexivity. }
               rewrite H7. rewrite removelast_app. right.
               rewrite in_list_in_sublits. left.
               rewrite in_list_in_sublits. right.
               simpl. left. reflexivity.
               congruence.
            -- inversion H0. congruence.
            -- inversion H0. congruence.
            -- inversion H0. congruence.
            -- inversion H0. congruence.
            --
               assert (MutexEnabled a1 s) as enabledA1. { auto. }
               rewrite nonBlockingSemantic in H2 ; auto. destruct H2.
               assert ((forall s s1 s2 : ModelState,
                           Semantic s a2 s1 /\ Semantic s a1 s2 ->
                           exists s' : ModelState, Semantic s1 a1 s' /\ Semantic s2 a2 s')).
               { intros. rewrite and_comm in H3. apply commute_actions in H3. destruct H3.
                 exists x0. rewrite and_comm in H3. assumption. }       
               eapply IHmutexIndependent in H3; eauto.
               destruct H3 as [IndepA1 [IndepA2 CommAA]].
               apply IndepA2 in H1. rewrite  H1 in enabledA1. assumption.  
             -- 
               assert (MutexEnabled a1 s') as enabledA1. { auto. }
               rewrite nonBlockingSemantic in H2 ; auto. destruct H2.
               assert ((forall s s1 s2 : ModelState,
                           Semantic s a2 s1 /\ Semantic s a1 s2 ->
                           exists s' : ModelState, Semantic s1 a1 s' /\ Semantic s2 a2 s')).
               { intros. rewrite and_comm in H3. apply commute_actions in H3. destruct H3.
                 exists x0. rewrite and_comm in H3. assumption. }       
               eapply IHmutexIndependent in H3; eauto.
               destruct H3 as [IndepA1 [IndepA2 CommAA]].
               apply IndepA2 in H1. rewrite <- H1 in enabledA1. assumption.  
               econstructor; eauto.
             -- 
               assert (MutexEnabled a2 s) as enabledA1. { auto. }
               rewrite nonBlockingSemantic in H2 ; auto. destruct H2.
               assert ((forall s s1 s2 : ModelState,
                           Semantic s a2 s1 /\ Semantic s a1 s2 ->
                           exists s' : ModelState, Semantic s1 a1 s' /\ Semantic s2 a2 s')).
               { intros. rewrite and_comm in H3. apply commute_actions in H3. destruct H3.
                 exists x0. rewrite and_comm in H3. assumption. }       
               eapply IHmutexIndependent in H3; eauto.
               destruct H3 as [IndepA1 [IndepA2 CommAA]].
               apply IndepA1 in H1. rewrite  H1 in enabledA1. assumption.  
             -- 
               assert (MutexEnabled a2 s') as enabledA1. { auto. }
               rewrite nonBlockingSemantic in H2 ; auto. destruct H2.
               assert ((forall s s1 s2 : ModelState,
                           Semantic s a2 s1 /\ Semantic s a1 s2 ->
                           exists s' : ModelState, Semantic s1 a1 s' /\ Semantic s2 a2 s')).
               { intros. rewrite and_comm in H3. apply commute_actions in H3. destruct H3.
                 exists x0. rewrite and_comm in H3. assumption. }       
               eapply IHmutexIndependent in H3; eauto.
               destruct H3 as [IndepA1 [IndepA2 CommAA]].
               apply IndepA1 in H1. rewrite <- H1 in enabledA1. assumption.  
               econstructor; eauto.
     - (* <-- *)
       intros.
       destruct action1; destruct action2;
         pose proof (Nat.eq_dec actor actor0) as diff_actor; destruct diff_actor;
         pose proof (Nat.eq_dec mutex mutex0) as diff_mutex; destruct diff_mutex;
         try (constructor; simpl in *; auto; fail);
         try (apply Symmetrical; apply LockUnlock + apply LockWait; simpl in *;
              auto; fail).
       -- (* LockLock same actor, same mutex  => not indep *)
         specialize (H InitialState). assert (reachableState InitialState) as reached . { constructor. }
         apply H in reached. destruct reached as [a1_invers_a2 [a2_invers_a1 commute]].
         rewrite <- e in *. rewrite <- e0 in *.
         assert (Semantic InitialState (MutexLock actor mutex) {| Ownership := fun m => if m=? mutex then Some actor 
                                                                                     else  None
                                                               ; Waiting := emptyWaiting|}).
         { constructor; simpl; auto with Mutex LTSGeneral. }
         apply a1_invers_a2 in H0 as absurdity.
         assert (Enabled (MutexLock actor mutex) InitialState). { apply nonBlockingSemantic; eauto. constructor. }
         rewrite absurdity in H1. inversion H1. simpl in *. rewrite Nat.eqb_refl in H4. congruence.
       -- (* LockLock diff actor, same mutex  => not indep *)
         specialize (H InitialState). assert (reachableState InitialState) as reached . { constructor. }
         apply H in reached. destruct reached as [a1_invers_a2 [a2_invers_a1 commute]].
         rewrite <- e in *. 
         assert ((Semantic InitialState (MutexLock actor mutex) {| Ownership := fun m => if m=? mutex then Some actor 
                                                                                     else  None
                                                                ; Waiting := emptyWaiting|})
                 /\
                 (Semantic InitialState (MutexLock actor0 mutex) {| Ownership := fun m => if m=? mutex then Some actor0 
                                                                                       else  None
                                                                 ; Waiting := emptyWaiting|})).
         { split; constructor; simpl; auto with Mutex LTSGeneral. }
         apply commute in H0 as absurdity. destruct absurdity. destruct H1. inversion H1; inversion H2;
         congruence.
       -- (* LockUnlock same actor, same mutex => not indep *)
         pose (s_after_one_lock := {| Ownership := fun m => if m=? mutex then Some actor 
                                              else  None
                        ; Waiting := emptyWaiting|}).
         specialize (H  s_after_one_lock).
         assert (reachableState s_after_one_lock) as reached . {
           unfold s_after_one_lock.
           apply Trans with (s:=InitialState)(a:=(MutexLock actor mutex)); constructor; autounfold with Mutex; simpl; 
             auto with Mutex LTSGeneral. }
         apply H in reached. destruct reached as [a1_invers_a2 [a2_invers_a1 commute]].
         rewrite <- e in *. rewrite <- e0 in *.
         assert (Semantic s_after_one_lock (MutexUnlock actor mutex) {| Ownership := emptyOwnership
                                                                     ; Waiting := emptyWaiting|}).
         { constructor; simpl; auto with Mutex LTSGeneral. }
         apply a2_invers_a1 in H0 as absurdity.
         assert (Enabled (MutexLock actor mutex) InitialState). {
           apply nonBlockingSemantic; try constructor.
           exists s_after_one_lock. constructor; unfold s_after_one_lock in *; simpl; auto with Mutex LTSGeneral.
         }
         unfold InitialState in H1.
         rewrite <- absurdity in H1. inversion H1. simpl in *. rewrite Nat.eqb_refl in H4. congruence.
       -- (* LockWait same actor, same mutex => not indep *)
         pose (s_after_one_lock := {| Ownership := fun m => if m=? mutex then Some actor 
                                              else  None
                        ; Waiting := emptyWaiting|}).
         specialize (H InitialState). assert (reachableState InitialState) as reached . { constructor. }
         apply H in reached. destruct reached as [a1_invers_a2 [a2_invers_a1 commute]].
         rewrite <- e in *. rewrite <- e0 in *.
         assert (Semantic InitialState (MutexLock actor mutex) s_after_one_lock).
         { constructor; simpl; auto with Mutex LTSGeneral. }
         apply a1_invers_a2 in H0 as absurdity.
         assert (Enabled (MutexWait actor mutex) s_after_one_lock). {
           constructor. unfold s_after_one_lock. simpl. auto with Mutex LTSGeneral. }
         rewrite <- absurdity in H1. inversion H1. simpl in *. unfold emptyOwnership in H5. congruence.
      -- (* UnlockLock same actor, same mutex => not indep *)
         pose (s_after_one_lock := {| Ownership := fun m => if m=? mutex then Some actor 
                                              else  None
                        ; Waiting := emptyWaiting|}).
         specialize (H  s_after_one_lock).
         assert (reachableState s_after_one_lock) as reached . {
           unfold s_after_one_lock.
           apply Trans with (s:=InitialState)(a:=(MutexLock actor mutex)); constructor; autounfold with Mutex; simpl; 
             auto with Mutex LTSGeneral. }
         apply H in reached. destruct reached as [a1_invers_a2 [a2_invers_a1 commute]].
         rewrite <- e in *. rewrite <- e0 in *.
         assert (Semantic s_after_one_lock (MutexUnlock actor mutex) {| Ownership := emptyOwnership
                                                                     ; Waiting := emptyWaiting|}).
         { constructor; simpl; auto with Mutex LTSGeneral. }
         apply a1_invers_a2 in H0 as absurdity.
         assert (Enabled (MutexLock actor mutex) InitialState). {
           apply nonBlockingSemantic; try constructor.
           exists s_after_one_lock. constructor; unfold s_after_one_lock in *; simpl; auto with Mutex LTSGeneral.
         }
         unfold InitialState in H1.
         rewrite <- absurdity in H1. inversion H1. simpl in *. rewrite Nat.eqb_refl in H4. congruence.
      -- (* UnlockUnlock same actor, same mutex => not indep *)
        pose (s_after_one_lock := {| Ownership := fun m => if m=? mutex then Some actor 
                                                        else  None
                                  ; Waiting := emptyWaiting|}).
        specialize (H  s_after_one_lock).
        assert (reachableState s_after_one_lock) as reached . {
          unfold s_after_one_lock.
          apply Trans with (s:=InitialState)(a:=(MutexLock actor mutex)); constructor; autounfold with Mutex; simpl; 
            auto with Mutex LTSGeneral. }
        apply H in reached. destruct reached as [a1_invers_a2 [a2_invers_a1 commute]].
        rewrite <- e in *. rewrite <- e0 in *.
        assert (Semantic s_after_one_lock (MutexUnlock actor mutex) {| Ownership := emptyOwnership
                                                                    ; Waiting := emptyWaiting|}).
        { constructor; simpl; auto with Mutex LTSGeneral. }
        apply a1_invers_a2 in H0 as absurdity.
        assert (Enabled (MutexUnlock actor mutex) s_after_one_lock). {
          constructor. unfold s_after_one_lock. simpl. auto with Mutex LTSGeneral.
        }
        rewrite absurdity in H1. inversion H1. simpl in H5. unfold emptyOwnership in H5. congruence.
      -- (* UnlockUnlock diff actor, same mutex => not indep *)
        pose (s_after_two_lock := {| Ownership := fun m => if m=? mutex then Some actor 
                                                        else  None
                                  ; Waiting := fun m => if m=? mutex then [actor0] 
                                                     else nil |}).
         pose (s_after_one_lock := {| Ownership := fun m => if m=? mutex then Some actor 
                                                        else  None
                                  ; Waiting := emptyWaiting|}).
        specialize (H  s_after_two_lock).
        assert (reachableState s_after_two_lock) as reached . {
          unfold s_after_two_lock. 
          apply Trans with (s:=s_after_one_lock)(a:=(MutexLock actor0 mutex)); unfold s_after_one_lock.
          apply SemanticLockOwned with (actor':=actor)(waiters:=nil); simpl in *; eauto with Mutex LTSGeneral.
          apply Trans with (s:=InitialState)(a:=(MutexLock actor mutex)); constructor; autounfold with Mutex; simpl;
            auto with Mutex LTSGeneral.
          }
        apply H in reached. destruct reached as [a1_invers_a2 [a2_invers_a1 commute]].
        rewrite <- e in *. 
        assert (Semantic s_after_two_lock (MutexUnlock actor mutex) {| Ownership := fun m => if m=? mutex
                                                                                          then Some actor0 
                                                                                          else  None
                                                                    ; Waiting := emptyWaiting|}).
        { unfold s_after_two_lock; apply SemanticUnlockWaiter with (fwait:=actor0)(waiters:=nil); simpl;
            auto with Mutex LTSGeneral. }
        apply a1_invers_a2 in H0 as absurdity.
        assert (Enabled (MutexUnlock actor0 mutex)
                {|
                  Ownership := fun m : MutexId => if m =? mutex then Some actor0 else None; Waiting := emptyWaiting
                |}). {
          constructor. simpl. auto with Mutex LTSGeneral.
        }
        rewrite <- absurdity in H1. inversion H1. simpl in H5. rewrite Nat.eqb_refl in H5. congruence.
      -- (* UnlockWait same actor, same mutex => not indep *)
         pose (s_after_one_lock := {| Ownership := fun m => if m=? mutex then Some actor 
                                                        else  None
                                  ; Waiting := emptyWaiting|}).
        specialize (H  s_after_one_lock).
        assert (reachableState s_after_one_lock) as reached . {
          unfold s_after_one_lock.
          apply Trans with (s:=InitialState)(a:=(MutexLock actor mutex)); constructor; autounfold with Mutex; simpl; 
            auto with Mutex LTSGeneral. }
        apply H in reached. destruct reached as [a1_invers_a2 [a2_invers_a1 commute]].
        rewrite <- e in *. rewrite <- e0 in *.
        assert (Semantic s_after_one_lock (MutexUnlock actor mutex) {| Ownership := emptyOwnership
                                                                    ; Waiting := emptyWaiting|}).
        { constructor; simpl; auto with Mutex LTSGeneral. }
        apply a1_invers_a2 in H0 as absurdity.
        assert (Enabled (MutexWait actor mutex) s_after_one_lock). {
          constructor. unfold s_after_one_lock. simpl. auto with Mutex LTSGeneral.
        }
        rewrite absurdity in H1. inversion H1. simpl in H5. unfold emptyOwnership in H5. congruence.
      -- (* UnlockWait diff actor; same mutex => not indep *)
         pose (s_after_two_lock := {| Ownership := fun m => if m=? mutex then Some actor 
                                                        else  None
                                  ; Waiting := fun m => if m=? mutex then [actor0] 
                                                     else nil |}).
         pose (s_after_one_lock := {| Ownership := fun m => if m=? mutex then Some actor 
                                                        else  None
                                  ; Waiting := emptyWaiting|}).
        specialize (H  s_after_two_lock).
        assert (reachableState s_after_two_lock) as reached . {
          unfold s_after_two_lock. 
          apply Trans with (s:=s_after_one_lock)(a:=(MutexLock actor0 mutex)); unfold s_after_one_lock.
          apply SemanticLockOwned with (actor':=actor)(waiters:=nil); simpl in *; eauto with Mutex LTSGeneral.
          apply Trans with (s:=InitialState)(a:=(MutexLock actor mutex)); constructor; autounfold with Mutex; simpl;
            auto with Mutex LTSGeneral.
          }
        apply H in reached. destruct reached as [a1_invers_a2 [a2_invers_a1 commute]].
        rewrite <- e in *. 
        assert (Semantic s_after_two_lock (MutexUnlock actor mutex) {| Ownership := fun m => if m=? mutex
                                                                                          then Some actor0 
                                                                                          else  None
                                                                    ; Waiting := emptyWaiting|}).
        { unfold s_after_two_lock; apply SemanticUnlockWaiter with (fwait:=actor0)(waiters:=nil); simpl;
            auto with Mutex LTSGeneral. }
        apply a1_invers_a2 in H0 as absurdity.
        assert (Enabled (MutexWait actor0 mutex)
                {|
                  Ownership := fun m : MutexId => if m =? mutex then Some actor0 else None; Waiting := emptyWaiting
                |}). {
          constructor. simpl. auto with Mutex LTSGeneral.
        }
        rewrite <- absurdity in H1. inversion H1. simpl in H5. rewrite Nat.eqb_refl in H5. congruence.
      -- (* WaitLock same actor, same mutex => not indep *)
        pose (s_after_one_lock := {| Ownership := fun m => if m=? mutex then Some actor 
                                              else  None
                        ; Waiting := emptyWaiting|}).
         specialize (H InitialState). assert (reachableState InitialState) as reached . { constructor. }
         apply H in reached. destruct reached as [a1_invers_a2 [a2_invers_a1 commute]].
         rewrite <- e in *. rewrite <- e0 in *.
         assert (Semantic InitialState (MutexLock actor mutex) s_after_one_lock).
         { constructor; simpl; auto with Mutex LTSGeneral. }
         apply a2_invers_a1 in H0 as absurdity.
         assert (Enabled (MutexWait actor mutex) s_after_one_lock). {
           constructor. unfold s_after_one_lock. simpl. auto with Mutex LTSGeneral. }
         rewrite <- absurdity in H1. inversion H1. simpl in *. unfold emptyOwnership in H5. congruence.
      -- (* WaitUnlock same actor, same mutex => not indep *)
         pose (s_after_one_lock := {| Ownership := fun m => if m=? mutex then Some actor 
                                                        else  None
                                  ; Waiting := emptyWaiting|}).
        specialize (H  s_after_one_lock).
        assert (reachableState s_after_one_lock) as reached . {
          unfold s_after_one_lock.
          apply Trans with (s:=InitialState)(a:=(MutexLock actor mutex)); constructor; autounfold with Mutex; simpl; 
            auto with Mutex LTSGeneral. }
        apply H in reached. destruct reached as [a1_invers_a2 [a2_invers_a1 commute]].
        rewrite <- e in *. rewrite <- e0 in *.
        assert (Semantic s_after_one_lock (MutexUnlock actor mutex) {| Ownership := emptyOwnership
                                                                    ; Waiting := emptyWaiting|}).
        { constructor; simpl; auto with Mutex LTSGeneral. }
        apply a2_invers_a1 in H0 as absurdity.
        assert (Enabled (MutexWait actor mutex) s_after_one_lock). {
          constructor. unfold s_after_one_lock. simpl. auto with Mutex LTSGeneral.
        }
        rewrite absurdity in H1. inversion H1. simpl in H5. unfold emptyOwnership in H5. congruence.
      -- (* UnlockWait diff actor; same mutex => not indep *)
         pose (s_after_two_lock := {| Ownership := fun m => if m=? mutex then Some actor0 
                                                        else  None
                                  ; Waiting := fun m => if m=? mutex then [actor] 
                                                     else nil |}).
         pose (s_after_one_lock := {| Ownership := fun m => if m=? mutex then Some actor0 
                                                        else  None
                                  ; Waiting := emptyWaiting|}).
        specialize (H  s_after_two_lock).
        assert (reachableState s_after_two_lock) as reached . {
          unfold s_after_two_lock. 
          apply Trans with (s:=s_after_one_lock)(a:=(MutexLock actor mutex)); unfold s_after_one_lock.
          apply SemanticLockOwned with (actor':=actor0)(waiters:=nil); simpl in *; eauto with Mutex LTSGeneral.
          apply Trans with (s:=InitialState)(a:=(MutexLock actor0 mutex)); constructor; autounfold with Mutex; simpl;
            auto with Mutex LTSGeneral.
          }
        apply H in reached. destruct reached as [a1_invers_a2 [a2_invers_a1 commute]].
        rewrite <- e in *. 
        assert (Semantic s_after_two_lock (MutexUnlock actor0 mutex) {| Ownership := fun m => if m=? mutex
                                                                                          then Some actor 
                                                                                          else  None
                                                                    ; Waiting := emptyWaiting|}).
        { unfold s_after_two_lock; apply SemanticUnlockWaiter with (fwait:=actor)(waiters:=nil); simpl;
            auto with Mutex LTSGeneral. }
        apply a2_invers_a1 in H0 as absurdity.
        assert (Enabled (MutexWait actor mutex)
                {|
                  Ownership := fun m : MutexId => if m =? mutex then Some actor else None; Waiting := emptyWaiting
                |}). {
          constructor. simpl. auto with Mutex LTSGeneral.
        }
        rewrite <- absurdity in H1. inversion H1. simpl in H5. rewrite Nat.eqb_refl in H5. congruence.
 Qed.

End MutexModel.
