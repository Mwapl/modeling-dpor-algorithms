Require Import Coq.Lists.ListDec.

From Coq Require Import Lists.List.
From Coq Require Import Arith.PeanoNat.
Import ListNotations.

Require Import Setoid.
Require Import Ring.
Require Import Ring_tac.

Require Import ComputationalModelFile.

Module LTS (Model : ComputationModel).

  Create HintDb LTS.
  
  Definition Process : Type  := list Model.Action.

  Definition ProgramCode : Type := Aid -> Process.
  
  Record State : Type := {
      ProgramState : ProgramCode;
      ModelData : Model.ModelState;
    }.


  Definition TransitionFunction (s s' : State) (act : Model.Action) : Prop :=
    exists t, (ProgramState s) (Model.Proc act) = act :: t
         /\ (ProgramState s') (Model.Proc act) = t
         /\ (forall actor2, actor2 <> Model.Proc act -> (ProgramState s) actor2 = (ProgramState s') actor2)
         /\ Model.Semantic (ModelData s) act (ModelData s').
  
  Notation " s '=>' a '=>' s' " := (TransitionFunction s s' a) (at level 40, a at next level).

  Hint Unfold TransitionFunction : LTS.

  Definition Transition : Type := (State * Model.Action * State).

  Definition action_of_transition (t : Transition) : Model.Action :=
    let '((_, a), _) := t in a.
  
  Inductive Sequence : State -> list Transition -> State -> Prop :=
  | OneStep (s s' : State) (action : Model.Action) :
    s => action => s' ->
                 Sequence s [(s, action, s')] s'
  | Trans (s s' s'' : State) (S1 S2: list Transition):
    Sequence s S1 s' ->
    Sequence s' S2 s'' ->
    Sequence s (S1++S2) s''.
   
  Notation " s '=[' seq ']=>' s' " := (Sequence s seq s') (at level 40, seq at next level).

  Definition InitialState (initialProg : Aid -> Process) : State :=
    {| ProgramState := initialProg; ModelData := Model.InitialState |}.

  Definition FinalState (s : State) : Prop :=
    forall a : Aid, ProgramState s a = nil.

  Definition Execution (exec : list Transition) : Prop :=
    exists initialProg : Aid -> Process, exists s : State,
      Sequence (InitialState initialProg) exec s.

  Definition MaximalExecution (exec : list Transition) : Prop :=
    exists initialProg : Aid -> Process, exists s : State,
      FinalState s /\
      Sequence (InitialState initialProg) exec s.
  
  Inductive reachableState : State -> Prop :=
  | Init (prog : Aid -> Process) :
    reachableState (InitialState prog)
  | Transitive (s s' : State) (a : Model.Action) :
    reachableState s ->
    s => a => s' ->
            reachableState s'.

  Lemma reachableState_imply_model_reachable_state :
    forall s, reachableState s -> Model.reachableState (ModelData s).
  Proof.
    intros. induction H.
    - simpl. constructor.
    - inversion H0. econstructor. destruct H1 as [_ [_ [_ semantic]]]. apply semantic.
      auto.
  Qed.

  Hint Resolve reachableState_imply_model_reachable_state : LTS.
    
  Definition Enabled (a : Model.Action) (s : State) : Prop
    :=
    exists s2 : State, s => a => s2.

  Hint Unfold Enabled : LTS.
  
  Lemma enabled_imply_model_enabled :
    forall s action, reachableState s -> Enabled action s -> Model.Enabled action (ModelData s).
  Proof.
    intros. autounfold with LTS in *. destruct H0 as [s2 [t [_ [_ [_ semantic]]]]].
    apply Model.nonBlockingSemantic; eauto with LTS.
  Qed.

  Hint Resolve enabled_imply_model_enabled : LTS.
  
  Definition Commute (a1 a2 : Model.Action) (s : State) : Prop
    := (reachableState s) ->
                (forall s' : State,  s => a1 => s' -> (Enabled a2 s <-> Enabled a2 s'))
                /\ (forall s' : State, s => a2 => s' -> (Enabled a1 s <-> Enabled a1 s'))
                /\ (forall s1 s2 : State,
                      (s => a1 => s1 /\ s => a2 => s2) ->
                      exists s' : State, (s2 => a1 => s' /\ s1 => a2 => s')).
  
  Definition Independent (a1 a2 : Model.Action) : Prop
    :=
    forall s: State, Commute a1 a2 s.

  Hint Unfold Commute : LTS.  
  Hint Unfold Independent : LTS.

  Theorem independent_symmetric : forall a1 a2,
      Independent a1 a2 -> Independent a2 a1.
  Proof.
    intros. 
    autounfold with LTS in *. intros.
    specialize (H s). destruct H. destruct H0.
    apply Init. auto. apply (Transitive s s' a); eauto.
    split; eauto.
    split; eauto.
    intros. destruct H1. apply H1 in H2. apply H2 in H3. assumption.
    destruct H1. apply H1 in H2. apply H2.
    split; eauto.
    destruct H1. intros. 
    rewrite and_comm in H3. apply H2 in H3. destruct H3. exists x. rewrite and_comm; auto.      
  Qed.

  Definition Dependent (a1 a2 : Model.Action) : Prop
    :=
    ~ Independent a1 a2.

  Lemma actor_persistence :
    forall s : State,
      reachableState s ->
      forall a1 a2 : Model.Action,
        Enabled a1 s ->
        Enabled a2 s ->
        Model.Proc a1 <> Model.Proc a2 -> forall s' : State,
            s => a1 => s' -> Enabled a2 s'.
  Proof.
    intros. 
    pose proof (Model.ActorPersistence) as model_persistence.
    specialize (model_persistence (ModelData s)). apply reachableState_imply_model_reachable_state in H as rState.
    apply model_persistence with (s':=ModelData s')(a1:=a1)(a2:=a2) in rState; auto with LTS.
    autounfold with LTS. autounfold with LTS in H1, H3. 
    destruct H3 as [rest_p1 [action_eq_p1 [rest_eq_p1 [other_eq_p1 semantic_p1]]]].
    destruct H1 as [state_p2' [rest_p2 [action_eq_p2 [rest_eq_p2 [other_eq_p2 semantic_p2]]]]].
    apply Model.nonBlockingSemantic in rState. destruct rState.
    exists {|
        ProgramState := fun actor => if actor =? Model.Proc a2 then rest_p2
                                  else if actor =? Model.Proc a1 then rest_p1
                                       else ProgramState s' actor; 
        ModelData := x;
      |}.
    exists rest_p2.
    repeat (split; simpl; auto with LTS LTSGeneral).
    econstructor; eauto with LTS.
    destruct H3 as [rest_p1 [action_eq_p1 [rest_eq_p1 [other_eq_p1 semantic_p1]]]].
    auto.
  Qed.
    
  Theorem different_process_independent : forall (p1 p2 : Aid) action1 action2,
      p1 <> p2 ->
      Model.Proc action1 = p1 ->
      Model.Proc action2 = p2 ->    
      Model.Independent action1 action2 ->
      Independent action1 action2.
  Proof.
    intros.
    unfold Independent. intros. split.
    - 
      intros. split.
      -- intro. apply actor_persistence with (s:=s)(a1:=action1); auto with LTS LTSGeneral. 
         econstructor; eauto.
      -- (* <-- *)
        intros. unfold Enabled in *. destruct H5.
        rewrite Model.indep_caracterization in H2.
        destruct H4 as [rest_p1 [action_eq_p1 [rest_eq_p1 [other_eq_p1 semantic_p1]]]].
        destruct H5 as [rest_p2 [action_eq_p2 [rest_eq_p2 [other_eq_p2 semantic_p2]]]].
        assert (Model.Enabled action2 (ModelData s')) as enabled_p2. {
          apply Model.nonBlockingSemantic; eauto.
          apply reachableState_imply_model_reachable_state in H3.
          econstructor; eauto.
        }
        destruct s. apply reachableState_imply_model_reachable_state in H3. simpl in H3.
        apply H2 in H3 as validModelState. destruct validModelState. simpl in *. apply H4 in semantic_p1.
 
        rewrite <- semantic_p1 in enabled_p2. apply Model.nonBlockingSemantic with (action := action2) in H3.
        rewrite H3 in enabled_p2. destruct enabled_p2.
        exists {|
            ProgramState := fun actor => if actor =? p2 then rest_p2
                                      else ProgramState0 actor; 
            ModelData := x0;
          |}.
        econstructor. simpl in *. rewrite H0 in *. rewrite H1 in *.
        split. apply Nat.neq_sym in H. apply other_eq_p1 in H. rewrite H in *. apply action_eq_p2.
        split. auto with LTSGeneral.
        split. auto with LTSGeneral.
        assert (exists s' : Model.ModelState, Model.Semantic ModelData0 action2 s'). { eauto. }
        rewrite <- H3 in H7. auto.
    -
      split. intros. split.
      --
        intro. apply actor_persistence with (s:=s)(a1:=action2); auto with LTS LTSGeneral. 
         econstructor; eauto.
      -- (* <-- *)
        intros. unfold Enabled in *. destruct H5.
        rewrite Model.indep_caracterization in H2.
        destruct H4 as [rest_p1 [action_eq_p1 [rest_eq_p1 [other_eq_p1 semantic_p1]]]].
        destruct H5 as [rest_p2 [action_eq_p2 [rest_eq_p2 [other_eq_p2 semantic_p2]]]].
        assert (Model.Enabled action1 (ModelData s')) as enabled_p2. {
          apply Model.nonBlockingSemantic; eauto.
          apply reachableState_imply_model_reachable_state in H3.
          econstructor; eauto.
        }
        destruct s. apply reachableState_imply_model_reachable_state in H3. simpl in *.
        apply H2 in H3 as validModelState. destruct validModelState as [_ [enabledness _]].
        apply enabledness in semantic_p1.
        rewrite <- semantic_p1 in enabled_p2. apply Model.nonBlockingSemantic with (action := action1) in H3.
        rewrite H3 in enabled_p2. destruct enabled_p2.
        exists {|
            ProgramState := fun actor => if actor =? p1 then rest_p2
                                      else ProgramState0 actor; 
            ModelData := x0;
          |}.
        econstructor. simpl in *. rewrite H0 in *. rewrite H1 in *. 
        split. apply other_eq_p1 in H. rewrite H in *. apply action_eq_p2.
        split. auto with LTSGeneral. 
        split. auto with LTSGeneral. 
        assert (exists s' : Model.ModelState, Model.Semantic ModelData0 action1 s'). { eauto. }
        rewrite <- H3 in H5. auto.
      -- (* commutativity *)
        intros.  rewrite Model.indep_caracterization in H2.
        destruct H4.
        destruct H4 as [rest_p1 [action_eq_p1 [rest_eq_p1 [other_eq_p1 semantic_p1]]]].
        destruct H5 as [rest_p2 [action_eq_p2 [rest_eq_p2 [other_eq_p2 semantic_p2]]]].
        specialize (H2 (ModelData s)). apply reachableState_imply_model_reachable_state in H3.
        apply H2 in H3. destruct H3 as [enabled1 [enabled2 commute]].
        assert (Model.Semantic (ModelData s) action1 (ModelData s1) /\
                  Model.Semantic (ModelData s) action2 (ModelData s2)). { split; auto. }
        apply commute in H3. destruct H3 as [s' [a1_after_a2 a2_after_a1]].
        exists {|
            ProgramState := fun actor => if actor =? p1 then rest_p1
                                      else if actor =? p2 then rest_p2
                                           else ProgramState s actor; 
            ModelData := s';
          |}. split; econstructor; rewrite H0 in *; rewrite H1 in *; simpl in *.
        --- split. apply other_eq_p2 in H. rewrite <- H. apply action_eq_p1.
            split. auto with LTSGeneral.
            split. intros. pose proof (Nat.eq_dec actor2 p2). destruct H4; auto with LTSGeneral.
            apply enabled2 in semantic_p2. auto.
        --- split. apply Nat.neq_sym in H. apply other_eq_p1 in H. rewrite <- H. apply action_eq_p2.
            split. auto with LTSGeneral.
            split. auto with LTSGeneral. 
            apply enabled1 in semantic_p1. auto.
  Qed.

  Inductive MazurkiewiczEquiv : list Transition -> list Transition -> Prop :=
  | EqualExecutions (u : list Transition) :
     MazurkiewiczEquiv u u
  | IntervertingIndependent (u1 u2: list Transition) (t1 t2 : Transition) :
    Independent (action_of_transition t1) (action_of_transition t2) ->
    MazurkiewiczEquiv (u1 ++ [t1;t2] ++ u2) (u1 ++ [t2;t1] ++ u2)
  | MTrans (u v w : list Transition) :
    MazurkiewiczEquiv u v ->
    MazurkiewiczEquiv v w ->
    MazurkiewiczEquiv u w
  .
  
  Notation " u 'simeq' v " := (MazurkiewiczEquiv u v) (at level 40).

  Definition MazurkiewiczTrace (u : list Transition) : list Transition -> Prop :=
    fun v => u simeq v.

  Notation " v 'inset [' u ']" := (MazurkiewiczTrace u v) (at level 10).

  Lemma equiv_symmetric :
    forall u v : list Transition,
      u simeq v -> v simeq u.
  Proof.
    intros. induction H.
    - constructor.
    - constructor. apply independent_symmetric. assumption.
    - apply (MTrans w v u); auto. 
  Qed.
  
End LTS.

Require Import MutexModelFile.

Module MutexLTS := LTS MutexModel.

Require Import Coq.Sets.Ensembles.

Module Type MCAlgorithm (Model : ComputationModel).

  Module LTSModel := LTS Model.
  Import LTSModel.
  
  Parameter Algorithm : ProgramCode -> Ensemble (list Transition).

  Parameter ExploreExecution :
    forall code, forall l,
      In (list Transition) (Algorithm code) l -> Execution l. 

  Parameter Soundness :
    forall code, forall u,
      MaximalExecution u ->
      exists v, In (list Transition) (Algorithm code) v /\
             u simeq v.

  Parameter Optimality :
    forall code, forall u v,
      u simeq v ->
      In (list Transition) (Algorithm code) u ->
      ~ In (list Transition) (Algorithm code) v.
  
End MCAlgorithm.
