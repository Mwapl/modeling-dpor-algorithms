Require Import ComputationalModelFile.

Module MessageModel.

  Create HintDb MPI.

  Definition Aid := nat.
  Definition MailboxId := nat.
  
  Inductive MPIAction : Type :=
  | Send (actor : Aid) (mailbox : MailboxId) (commid : nat)
  | Recv (actor : Aid) (mailbox : MailboxId) (commid : nat)
  | WaitAny (actor : Aid) (commids : list nat)
  .
  
  Definition Action := MPIAction.
  
  Definition Proc (a : Action) : Aid :=
    match a with
    | Send a _ _ => a
    | Recv a _ _ => a
    | WaitAny a _ => a
    end
  .

  Inductive MPIRequest : Type :=
  | SendReq (actor : Aid) (commid : nat)
  | RecvReq (actor : Aid) (commid : nat)
  .
  
  Record MPIState :=  {
        Mailboxes : MailboxId -> list MPIRequest ;
        Satisfied : nat -> Prop ; (* Property that states which commid have been satisfied *)
  }
  .
  
  Definition ModelState := MPIState.

  Inductive MPIEnabled : Action -> ModelState -> Prop :=
  | Enabled a m s :
    (Ownership s) m <> Some a ->
    ~ (List.In a (Waiting s m))  ->
    MutexEnabled (MutexLock a m) s  
  | EnabledUnlock a m s :
    (Ownership s) m = Some a ->
    MutexEnabled (MutexUnlock a m) s
  | EnabledWait a m s :
    (Ownership s) m = Some a ->
    MutexEnabled (MutexWait a m) s              
  .

  Definition Enbaled := MPIEnabled.

  Inductive Action : Type :=
  | Send (.
  Parameter Proc : Action -> Aid.
  Parameter ModelState : Type.
  Parameter Enabled : Action -> ModelState -> Prop.
  Parameter Semantic : ModelState -> Action -> ModelState -> Prop.
  Parameter InitialState : ModelState.

  Inductive reachableState : ModelState -> Prop :=
  | Init :
    reachableState InitialState
  | Trans (s s' : ModelState) (a : Action) :
    Semantic s a s' ->
    reachableState s ->
    reachableState s'
  .

  Parameter nonBlockingSemantic :
    forall action : Action, forall s : ModelState,
      reachableState s ->
      ( Enabled action s <->
          exists s' : ModelState, Semantic s action s').

  Parameter ActorPersistence :
    forall s : ModelState,
      reachableState s ->
      forall a1 a2 : Action,
        Enabled a1 s ->
        Enabled a2 s ->
        Proc a1 <> Proc a2 ->
        forall s' : ModelState,
          Semantic s a1 s' ->
          Enabled a2 s'.
        
  Parameter Independent : Action -> Action -> Prop.
  Parameter indep_sym :
    forall a1 a2, Independent a1 a2 -> Independent a2 a1.
    
  Parameter indep_caracterization :
    forall a1 a2,
      Independent a1 a2 <->    
        (forall s: ModelState, (reachableState s) ->
                          (forall s' : ModelState,  Semantic s a1 s' -> (Enabled a2 s  <-> Enabled a2 s'))
                          /\ (forall s' : ModelState, Semantic s a2 s' -> (Enabled a1 s <-> Enabled a1 s' ))
                          /\ (forall s1 s2 : ModelState,
                                (Semantic s a1 s1 /\ Semantic s a2 s2) ->
                                exists s' : ModelState, (Semantic s2 a1 s' /\ Semantic s1 a2 s'))).
  
    
End MessageModel.
