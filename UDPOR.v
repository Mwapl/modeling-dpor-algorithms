Require Import Coq.Classes.RelationClasses.
Require Import Coq.Sets.Ensembles.

Require Import Coq.Lists.ListDec.
Require Import Coq.Lists.ListSet.
From Coq Require Import Lists.List.
From Coq Require Import Arith.PeanoNat.
Import ListNotations.

Require Import ComputationalModelFile.

Reserved Notation " e '<' e' " (at level 70).
Reserved Notation " e '#' e' " (at level 70).

Module Type UnfoldingEventType.
  Definition t : 

Module PrimeEventStructure (Model : ComputationModel).

  
  Definition Sigma := Model.Action.
  
  Record PES : Type := {
      EventRestriction : EventT -> Prop;
      Event : Type := {e : EventT | EventRestriction e};
      Causality : Event -> Event -> Prop where " e '<' e' " := (Causality e e');
      Lambda : Event -> Sigma;
      Conflict : Event -> Event -> Prop where " e # e' " := (Conflict e e');
      Valid_Causality : StrictOrder Causality;
      Valid_Conflict : Irreflexive Conflict /\ Symmetric Conflict;
      conflict_inherithed_by_causality : forall e e' e'' : Event, e # e' -> e' < e'' -> e # e'';
    }.

  Notation " e '<' e'" := ((Causality _) e e') (at level 70).
  Notation " e '#' e'" := ((Conflict _) e e') (at level 70).  

  Inductive EmptyEventRestriction : EventT -> Prop :=.
  Definition EmptyEvent : Type := {e : EventT | EmptyEventRestriction e}.
  Definition EmptyCausality : EmptyEvent -> EmptyEvent -> Prop. Proof. intros. destruct X. destruct e. Qed.
  Definition EmptyLambda : EmptyEvent -> Sigma. Proof. intros. destruct X. destruct e. Qed.
  Definition EmptyConflict : EmptyEvent -> EmptyEvent -> Prop. Proof. intros. destruct X. destruct e. Qed.
  Lemma EmptyValid_Causality : StrictOrder EmptyCausality.
  Proof.
    constructor.
    - unfold Irreflexive. unfold Reflexive. unfold complement.
      intros. destruct x. destruct e.
    - unfold Transitive. intros. destruct x. destruct e.
  Qed.
  Lemma EmptyValid_Conflict : Irreflexive EmptyConflict /\ Symmetric EmptyConflict.
  Proof.
    constructor.
    - unfold Irreflexive. unfold Reflexive. intros. destruct x. destruct e. 
    - unfold Symmetric. intros. destruct x. destruct e.
  Qed.
  Lemma Emptyconflict_inherithed_by_causality : forall e e' e'' : EmptyEvent,
      EmptyConflict e e' -> EmptyCausality e' e'' -> EmptyConflict e e''.
  Proof.
    intros. destruct e. destruct e.
  Qed.
  
  Definition EmptyPES : PES :=
    {|
      EventRestriction := EmptyEventRestriction;
      Causality := EmptyCausality;
      Lambda := EmptyLambda;
      Conflict := EmptyConflict;
      Valid_Causality := EmptyValid_Causality;
      Valid_Conflict := EmptyValid_Conflict;
      conflict_inherithed_by_causality := Emptyconflict_inherithed_by_causality;
    |}.
  
  Definition Causes (E : PES) (e : Event E) :
    Event E -> Prop :=
    fun (e' : Event E) => e' < e.
  
  Definition legalConfiguration (E : PES) (C : Event E  -> Prop) : Prop :=
    forall e e': Event E, 
      C e  -> ((Causes E e) e' -> C e') /\ (* causaly closed *)
               (C e' -> ~ (e # e'))
  .
  
  Definition Configuration (E : PES) : Type := 
    { C : Event E -> Prop | legalConfiguration E C }.

  Definition maxEvents (E : PES) (C : Configuration E) : Event E -> Prop :=
    fun (e : Event E) =>
      (proj1_sig C) e /\ ~ (exists e', (proj1_sig C) e' /\ e < e').

  Definition LocalConf (E : PES) (e : Event E) : Event E -> Prop :=
    fun (e' : Event E) => e' = e \/ (Causes E e) e'.

  Definition MaximalConfiguration (E : PES) : Configuration E -> Prop :=
    fun (C : Configuration E) =>
      forall e : Event E,
        proj1_sig C e \/ ~ legalConfiguration E (Union (Event E) (proj1_sig C) (Singleton (Event E) e)). 

  Inductive ImmediateConflict (E : PES) : Event E -> Event E -> Prop :=
    Immediate (e e' : Event E) :
      e # e' ->
      legalConfiguration E (Union (Event E) (Causes E e) (LocalConf E e')) ->
      legalConfiguration E (Union (Event E) (Causes E e') (LocalConf E e)) ->
      ImmediateConflict E e e'.

  Definition extension (E : PES) (C : Configuration E) : Event E -> Prop :=
    fun (e : Event E) =>
      ~ (proj1_sig C e) /\ (forall e' : Event E, Causes E e e' -> proj1_sig C e').

  Definition list_of_config (E : PES) (C : Configuration E) : list (Event E) -> Prop :=
    fun (config_list : list (Event E)) =>
      forall e : Event E, proj1_sig C e <-> In e config_list.

  Definition linearization_of_config  (E : PES) (C : Configuration E) : list (Event E) -> Prop :=
    fun (linearization : list (Event E)) =>
      (list_of_config E C linearization) /\
        (forall e e' : Event E,
            proj1_sig C e ->
            proj1_sig C e' ->
            e < e' ->
            exists l1 l2 l3 : list (Event E), linearization = l1 ++ [e] ++ l2 ++ [e'] ++ l3).
  
  Definition interleavings (E : PES) (C : Configuration E) : list Sigma -> Prop :=
    fun (linearization : list Sigma) =>
      exists l : list (Event E), 
      (linearization_of_config E C l) /\ linearization = map (Lambda E) l.

  (* Definition Prefix (E E' : PES) : Prop := *)
  (*   (forall e : EventT, EventRestriction E e -> EventRestriction E' e) /\ *)
  (*     (forall e e' : Event E, EventRestriction E e -> *)
  (*                       EventRestriction E e' -> *)
  (*                       Causality E e e' -> *)
  (*                       Causality E' e e'). *)
                        
      
End PrimeEventStructure.

Module Unfolding (Model : ComputationModel).

  Module SigmaPES := PrimeEventStructure Model.
  Import SigmaPES.

  
  
End Unfolding.  

       
       
